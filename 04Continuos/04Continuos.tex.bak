\documentclass[11pt,spanish,a4paper]{article}

\input{../introFCEyN}

\begin{document}
\begin{center}
\textbf{Física 2} (Físicos) \hfill \textcopyright {\tt DF, FCEyN, UBA}\\
%	\textsc{\large Física 2 (Físicos)} - Prof. Diana Skigin - 2"o cuat. 2014\\
%	\textsc{\large Primer Cuatrimestre - 2014}\\
	\textsc{\LARGE Sistemas continuos}
% \textsc{\large Guía 1:} Oscilaciones libres y forzadas
\end{center}

Los ejercicios con (*) son opcionales.


\begin{enumerate}

\section*{Modos normales}


\subsection*{Cuerda}

\item Se tiene una cuerda de longitud $L$ y densidad lineal de masa $\mu$ sometida a una tensión $T_{0}$.
Proponga como solución de la ecuación de ondas para un modo normal a la expresión: $\Psi(x,t)= A \sen{ \left(k x+ \varphi \right ) } \cos{ \left( \omega t + \theta \right) }$.
Tome el sistema de coordenadas con $x=0$ en un extremo de la cuerda y $x=L$ en el otro.
Encuentre la forma particular que adopta la solución propuesta en los siguientes casos: 
\begin{enumerate}
	\item $\Psi(0,t) = \Psi(L,t) = 0$ (ambos extremos están fijos). 
	\item $\Psi(0,t) = 0$ y $\pdv{\Psi}{x} (L,t)= 0$ (un extremo está fijo y el otro está libre).
	% \item $\Psi(0,t)= 0$ y $\frac{\partial \Psi}{\partial x}(L,t)= 0$ (un extremo está fijo y el otro está libre).
	¿Imponer que un extremo se encuentre ``libre'' es equivalente a no imponer condiciones de contorno sobre ese extremo?
	¿Cómo lograría un extremo ``libre'' para la cuerda? 
	\item $\pdv{\Psi}{x} (0,t)= \pdv{\Psi}{x} (L,t)= 0$ (ambos extremos se encuentran libres).
	% \item $\frac{\partial \Psi}{\partial x}(0,t)= \frac{\partial\Psi}{\partial x}(L,t)=0$ (ambos extremos se encuentran libres).
	¿A qué corresponde el modo de frecuencia mínima?
	¿Cuál es la frecuencia de oscilación de ese modo? 
	\item Ahora tome un sistema de coordenadas con $x=0$ en el centro de la cuerda.
	Halle la forma que adopta la solución general propuesta si $\Psi(-L/2,t)= \Psi(L/2,t)= 0$ (ambos extremos fijos).
\end{enumerate}



\item Se tiene una cuerda de \SI{20}{\centi\metre} de longitud y \SI{5}{\gram} de masa, sometida a una tensión de \SI{120}{\newton}.
Calcule sus modos naturales de oscilación.
¿Son todos audibles para el oído humano (\(\SI{20}{\hertz} < \nu < \SI{20}{\kilo\hertz}\))?



\item Consideremos que las cuatro cuerdas de un violín son de igual longitud, y que emiten en su modo fundamental las notas: sol$_{\text{2}}$
(198/s); re$_{\text{3}}$ (297/s); la$_{\text{3}}$ (440/s) y mi$_{\text{4}}$ (660/s).
De la primera a la cuarta las cuerdas son de distinto material y diámetro:
\begin{tasks}[style=enumerate](2)
	\task \isotope{Al} \(\rho= \SI{2.6}{\gram\over\centi\metre\cubed}\), \(d_1= \SI{0,09}{\centi\metre}\)
	\task Aleación \isotope{Al}-\isotope{Ni} $\rho = \SI{1.2}{\gram\over\centi\metre\cubed}$, $d_2 = \SI{0.12}{\centi\metre}$
	\task Aleación \isotope{Al}-\isotope{Ni} $\rho = \SI{1.2}{\gram\over\centi\metre\cubed}$, $d_3 = \SI{0.1}{\centi\metre}$
	\task Acero $\rho = \SI{7.5}{\gram\over\centi\metre\cubed}$, $d_4 = \SI{0.1}{\centi\metre}$.
\end{tasks}
Calcular las tensiones a las que deben estar sometidas con respecto a la primera.


\item Una cuerda de longitud $L$ fija en sus extremos es lanzada a oscilar con igual amplitud en sus dos modos de menor frecuencia.
Considere que parte del reposo. 
\begin{enumerate}
	\item Encuentre el apartamiento del equilibrio para cada punto de la cuerda en función del tiempo.
	\item ¿Con qué período se repite el movimiento?
	\item Grafíquelo para cuatro instantes equispaciados dentro de un período. 
\end{enumerate}



\subsection*{Gas en un tubo unidimensional}

\item Se tiene un tubo de longitud $L$. Considere las siguientes posibilidades: 
\begin{itemize}
	\item Está cerrado en ambos extremos, lleno de aire en su interior.
	\item Tiene un extremo cerrado y el otro abierto. 
	\item Ambos extremos están abiertos.
\end{itemize}
Asuma conocida $v_\text{sonido}$, $L$, la presión atmósferica $p_0$, $\rho_{0}= \frac{ \gamma p_0 }{ v_\text{sonido}^2}$ y que $\gamma= \frac{C_p}{C_v}= \frac{7}{5}$ para un gas diatómico.
Halle para cada una de dichas situaciones: 
\begin{enumerate}
	\item Las posibles longitudes de onda con las que puede vibrar el aire en el tubo, y sus correspondientes frecuencias. 
	\item Elija un sistema de referencia conveniente, y escriba la expresión más general para el desplazamiento de las partículas $\Psi(x,t)$.
	¿Qué parámetros conoce de dicha expresión?
	¿De qué dependen los que no conoce? 
	\item A partir de la expresión de $\Psi(x,t)$, hallar el apartamiento de la presión respecto a la atmosférica $\delta p(x,t)$.
	¿Cuál es la diferencia de fase entre ellas?
	¿Cuánto vale la amplitud de presión? 
	\item Hallar la función de densidad $\rho(x,t)$.
	¿Cuánto vale su amplitud?
\end{enumerate}



\item 
\begin{enumerate}
\item ¿Qué longitud debe tener un tubo de órgano abierto en ambos extremos para que produzca en el aire un sonido de \SI{440}{\hertz}? 
\item ¿Qué longitud deberá tener un tubo de órgano cerrado en uno de su extremos para que produzca el mismo tono en su primer armónico?
\end{enumerate}


\item Se tiene un tubo cerrado en uno de sus extremos; su longitud es menor a \SI{1}{\metre}.
Se acerca al extremo abierto un diapasón que está vibrando con $\nu = \SI{440}{\hertz}$.
Considere $v_\text{sonido} = \SI{330}{\metre\over\second}$.
\begin{enumerate}
\item Hallar las posibles longitudes del tubo para que haya resonancia.
Para cada una de ellas, ¿en qué modo está vibrando el aire contenido en el tubo? 
\item Repita lo anterior para un tubo abierto en ambos extremos.
\end{enumerate}


\section*{Ondas viajeras}

\subsection*{Estacionarias en una cuerda como superposición de viajeras}

\item 
\begin{minipage}[t][2cm]{0.6\textwidth}
Se tiene una cuerda de longitud $L=0,6\unit{\, m}$, fija en sus dos extremos, que se encuentra oscilando en uno de sus modos normales como se muestra en la figura.
La velocidad de propagación den dicha cuerda es \(v=80 \unit{m/s}\).
\end{minipage}
\begin{minipage}[c][1.5cm][t]{0.34\textwidth}
	\includegraphics[width=\textwidth]{ej1-32}
\end{minipage}
\begin{enumerate}
	\item Escribir $\Psi(x,t)$ (la elongación en un punto de la cuerda), sabiendo que a $t=0$ la elongación de todos los puntos es nula, que la amplitud pico a pico máxima es \(8\unit{mm}\), y que $\dot{\Psi}(L/2,0) > 0$
	\item Hallar $\Psi_1 (x-vt)$ y $\Psi_2 (x+vt)$ tales que $\Psi(x,t) = \Psi_1 (x-vt) + \Psi_2 (x+vt)$,
es decir: escribir a $\Psi(x,t)$ como la superposición de dos ondas viajeras.
\end{enumerate}

\item 
\begin{minipage}[t][2.6cm]{0.6\textwidth}
Se tiene una cuerda de longitud $L = \SI{1}{\metre}$, con un extremo fijo y uno libre, oscilando en el modo normal que se muestra en la figura.
La velocidad de propagación de las ondas en dicha cuerda es \(v= \SI{80}{\metre\over\second}\), y el desplazamiento de las partículas a $t=0$ es el máximo posible para este modo, siendo $\Psi(L,0) > 0$.
La amplitud pico a pico máxima es de \SI{8}{\milli\metre}.
\end{minipage}
\begin{minipage}[c][0.4cm][t]{0.34\textwidth}
	\includegraphics[width=\textwidth]{ej1-33}
\end{minipage}
\begin{enumerate}
	\item Resolver, para esta situación, todo lo pedido en el problema anterior. 
	\item Si ahora la cuerda está oscilando en un modo normal arbitrario $n$, con las mismas condiciones iniciales dadas arriba, repetir (a) (expresar en función de $n$).
\end{enumerate}



\subsection*{Parámetros de una onda viajera}


\item (*) Verifique si las siguientes expresiones matemáticas cumplen la ecuación
de las ondas unidimensional. Grafique las funciones dadas.
\begin{tasks}(3)
	\task $\Psi(x,t)=Ae^{-\lambda(x-vt)^{2}}$
	\task $\Psi(x,t)=\beta(x+vt)$
	\task $\Psi(x,t)=A\sin\left[k(x-vt)\right]$
	\task $\Psi(x,t)=B\sin^{2}\left(kx-\omega t\right)$
	\task $\Psi(x,t)=C\cos(kx)\sin(\omega t)$
	\task $\Psi(x,t)=De^{i(kx-\omega t)}$
\end{tasks}



\item La ecuación de una onda transversal en una cuerda está dada por: $y(x,t)=0,1\unit{\, m}\sin\left[\pi\left(x\unit{\, m^{-1}- 4 t \unit{\, s^{-1}}}\right)\right]$
($x$ e $y$ en metros y $t$ en segundos).
Determine:
\begin{enumerate}
\item La amplitud de la onda.
\item La frecuencia de vibración de la cuerda.
\item La velocidad de propagación de la onda.
\item En $t=1$ s, evaluar el desplazamiento, la velocidad y la aceleración
de un segmento pequeño de cuerda ubicado en $x=2$ m.
\end{enumerate}


\item Sea una onda transversal plana y armónica, cuya frecuencia angular vale $\omega=10\mbox{ s}^{-1}$ y cuyo número de onda es $k=100\mbox{\,\ m}^{-1}$.
En $x_{1}=1$ km y $t_{1}=1$ s la fase de la onda es $\nu(1\unit{\, km},1\unit{\, s})=3\pi/2$.
\begin{enumerate}
\item ¿Cuánto vale la fase en $x_{1}$ para $t=0$ s?
\item Considerando que $\nu(x,t)=kx-\omega t+\nu_{0}$, ¿cuánto vale $\nu_{0}$?
\item ¿A qué velocidad se propaga la onda?
\item ¿Cuánto tiempo debe transcurrir para que el frente de onda que se
hallaba en $x_{1}$ llegue a $x=2x_{1}$?
\end{enumerate}


\item Una cuerda con densidad lineal $\mu = \SI{0.005}{\kilo\gram\over\metre}$ se tensa aplicando una fuerza de \SI{0.25}{\newton}.
El extremo izquierdo se mueve hacia arriba y hacia abajo con un movimiento armónico simple de período \SI{0.5}{\second} y amplitud \SI{0.2}{\metre} mientras se mantiene la tensión constante.
Encontrar:
\begin{enumerate}
\item La velocidad de la onda generada en la cuerda, la frecuencia y la longitud de onda.
\item La expresión matemática para el desplazamiento: $y(x,t)$.
\item La energía cinética media por unidad de longitud, de una partícula del medio.
\item La energía potencial media por unidad de longitud, de una partícula.
\end{enumerate}



\subsection*{Reflexión y transmisión}

\item Nos interesa estudiar la unión de dos cuerdas de distinta densidad lineal $\rho_1$ y $\rho_2$, por lo que las consideraremos semi--infinitas. 
Mientras se las somete a una tensión $T$ constante incide en la primera una onda $\phi_{i}(x,t)=A_{i}\cos\left(k_{1}x-\omega t\right)$.
Se conocen: $\rho_{1}$, $\rho_{2}$, $T$, $\omega$ y $A_{i}$.
\begin{enumerate}
\item Calcule $k_{1}$ y $k_{2}$, es decir, los números de onda de cada lado de la unión.
\item Plantee la solución más general para $\phi(x,t)$ de cada lado de la unión.
\item ¿Qué condiciones deben verificarse en el punto de unión de las cuerdas?
\item Usando b) y c), calcule la perturbación $\phi(x,t)$ en cada una de las cuerdas.
\end{enumerate}


\item
\begin{minipage}[t][3.3cm]{0.6\textwidth}
Como nos interesa estudiar la unión de dos caños cuadrados de área transversal $A_1$ y $A_2$ los consideramos semi--infinitos.
Desde el izquierdo incide una onda acústica $\delta p_i (x,t) = a_i \cos{ \left( k_1 x - \omega t \right) }$.
Suponga despreciables los efectos de la viscosidad y dé por conocidos $A_{1}$, $A_{2}$, presión media $P_{0}$, densidad media $\rho_{0}$, $v_{s}$, $\omega$, $a_i$.
Halle amplitudes de presión y desplazamiento de moléculas a causa de las ondas reflejadas y transmitidas.
\end{minipage}
\begin{minipage}[c][0cm][t]{0.34\textwidth}
	\includegraphics[width=\textwidth]{ej2-10}
\end{minipage}


\item 
\begin{minipage}[t][1.6cm]{0.6\textwidth}
A este armado con idénticas áreas del problema anterior incide la misma onda.
Halle $\delta p(x,t)$ y $\Psi(x,t)$ en cada tramo.
\end{minipage}
\begin{minipage}[c][1cm][t]{0.34\textwidth}
	\includegraphics[width=\textwidth]{ej2-11}
\end{minipage}


\item
Desde el aire incide en dirección perpendicular a una superficie calma de agua una onda de sonido plana $\delta p_i (y,t) = A_i \cos{\left( k_i y - \omega t \right)}$.
Hallar las ondas reflejadas y transmitidas $\delta p_{r}(y,t)$ y $\delta p_{t}(y,t)$.
\begin{figure}[H]
\centering{}\includegraphics[clip,scale=0.25]{ej2-12}
\end{figure}


\end{enumerate}

\end{document}
